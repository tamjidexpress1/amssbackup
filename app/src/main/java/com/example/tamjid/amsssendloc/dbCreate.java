package com.example.tamjid.amsssendloc;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Tamjid on 9/26/2016.
 */
public class dbCreate extends SQLiteOpenHelper {
    public static final String DB_NAME = "LocAppDb";
    public static final String TB_name = "LocAppTb";
    public static final String BFTB_name = "bufferTb";
    public static int DB_VER = 1;
    public static final String UID = "_ID";
    public static final String UID2 = "_ID";
    public static final String EMPID = "empid";
    public static final String UNITID = "unitid";
    public static final String UNITID2 = "unitid";
    public static final String MACID = "unitid";
    public static final String TRANS = "trans";
    public static final String TIME = "time";
    public static final String LATITUDE="latitude";
    public static final String LONGITUDE="longitude";
    private static final String crtb = "CREATE TABLE " + TB_name + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + EMPID + " VARCHAR(255)," + UNITID + " VARCHAR(255),"+ LATITUDE + " VARCHAR(255),"+LONGITUDE+" VARCHAR(255));";
    public static final String createTB="CREATE TABLE " + BFTB_name + " (" + UID2 + " INTEGER PRIMARY KEY AUTOINCREMENT, " + MACID + " VARCHAR(255)," + UNITID2 + " VARCHAR(255),"+ TRANS + " VARCHAR(255),"+TIME+" VARCHAR(255));";
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TB_name;
    private static dbCreate instance = null;
    Context context;
    private String Tag="newtag";
    public dbCreate(Context context) {
        super(context, DB_NAME, null, DB_VER);
        this.context = context;
        Log.d(Tag, "dbcreates constructor is called ");
    }
    public static dbCreate getInstance(Context context) {
        if(instance == null) {
            instance = new dbCreate(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(Tag, "oncreate called");
        Toast.makeText(context, "create called", Toast.LENGTH_LONG).show();
        try {
            db.execSQL(crtb);
           // db.execSQL(createTB);
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(Tag,"problem creating the buffer databases");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL(DROP_TABLE);
            onCreate(db);
            Log.d(Tag, "onUpgrade called");
            Toast.makeText(context, "upgrade called", Toast.LENGTH_LONG).show();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void recreteDB(SQLiteDatabase db){
        db.execSQL(DROP_TABLE);
        onCreate(db);
        Log.d(Tag, "onUpgrade called");
        Toast.makeText(context, "Recreate called", Toast.LENGTH_LONG).show();
    }
}
