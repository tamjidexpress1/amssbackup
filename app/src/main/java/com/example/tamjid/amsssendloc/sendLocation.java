package com.example.tamjid.amsssendloc;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Tamjid on 9/28/2016.
 */
public class sendLocation extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {
    dbCreate dbcreate;
    SQLiteDatabase db;

    //bufferLogdb bufferrd;
   // SQLiteDatabase bufferdb;
    private String empId;
    public static final float GEOFENCE_RADIUS_IN_METERS = 500;
    protected ArrayList<Geofence> mGeofenceList;
    protected GoogleApiClient mGoogleApiClient;
    public static final HashMap<String, LatLng> LANDMARKS = new HashMap<String, LatLng>();
    private ScheduledExecutorService scheduledExecutorService;
    //private String logUrl="https://abdulhalim.pythonanywhere.com/rams/default/get_att_log.json";
   private String logUrl="https://abdulhalim.pythonanywhere.com/rams4/default/get_att_log.json";
    public String Tag="newtag";
    private boolean myDeletePermission=false;

    dbUnitSuper dbunit;
    public String unitUri="https://abdulhalim.pythonanywhere.com/rams4/default/getUnits.json?client_id=";
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //buildGoogleApiClient();
        mGoogleApiClient=new GoogleApiClient.Builder(getApplicationContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        //dbcreate=dbCreate.getInstance(this);
       // db=dbcreate.getWritableDatabase();
        //bufferrd=new bufferLogdb(this);
        //bufferdb=bufferrd.getWritableDatabase();
         dbunit=dbUnitSuper.getInstance(sendLocation.this);
        empId=dbunit.readEmpId();
        mGeofenceList = new ArrayList<Geofence>();
       populateGeofenceList();
Log.d(Tag,"populate geofencelist is called on oncreate of service");
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
       scheduledExecutorService.scheduleWithFixedDelay(startSend,2,10, TimeUnit.MINUTES);

    }
Runnable startSend=new Runnable() {
    @Override
    public void run() {
        Log.d(Tag,"runnable is running");
        //String[] projection={dbcreate.UNITID};
        //Cursor C=db.query(dbcreate.TB_name,projection,null,null,null,null,null);
        ArrayList<String> unitIds=new ArrayList<>();

        dbBufferSuper dbBuffer=dbBufferSuper.getInstance(sendLocation.this);

        unitIds=dbunit.readUnitIds();


if(isOnLine())
        { for(int i=0;i<unitIds.size();i++) {
            String loopUnitID = unitIds.get(i);
            JSONObject unitJson = dbBuffer.readBufferLog(loopUnitID);

            if (unitJson != null) {
                sendLog send = new sendLog();
                send.execute(unitJson);
            } else {
                Log.d(Tag, "unitjson returns null for unit "+i);
            }

        }


        getUnitUpdate unitUpdate=new getUnitUpdate();
        unitUpdate.execute(empId);}
        else {Log.d(Tag,"device is offline so noo data sent");}





        /*try {
            if (C.moveToFirst())do{
                //unit data query loop
                String loopUnitID=C.getString(C.getColumnIndex(dbcreate.UNITID));
                Log.d(Tag,"loopunitid is "+loopUnitID);
                String[] bufferSelection={loopUnitID};
                JSONObject unitJson=new JSONObject();
                JSONObject logJson=new JSONObject();

                Cursor bufferCursor=bufferdb.query(bufferrd.TB_name,null,bufferrd.UNITID+" =?",bufferSelection,null,null,null,null);
            if(bufferCursor.getCount()>0){
                //buffer data query loop
                Log.d(Tag,"some data is read from buffer database");
                bufferCursor.moveToFirst();
                int a=0;
                do{
                    String key =String.format("%s",a);
                    JSONObject value=new JSONObject();
                   String macid=bufferCursor.getString(bufferCursor.getColumnIndex(bufferrd.MACID));
                    String trans =bufferCursor.getString(bufferCursor.getColumnIndex(bufferrd.TRANS));
                    String datetime=bufferCursor.getString(bufferCursor.getColumnIndex(bufferrd.TIME));
                    //entry logs in a json object for a given unit
                    value.put("PIN",macid);
                    value.put("DateTime",datetime);
                    value.put("Verified","1");
                    value.put("Status","0");
                    value.put("WorkCode","0");
                    logJson.put(key,value);
                    //this is the last line
                    a++;
                }
                while (bufferCursor.moveToNext());
                //buffer data query ends here for a single unit
                //write code for each unit.write the complete json object for a unit here
                logJson.put("unit_id",loopUnitID);
                unitJson.put("Row",logJson);
                Log.d(Tag,"unitjson is "+unitJson.toString());
                sendLog send=new sendLog();
                send.execute(unitJson);

            }else {
                Log.d(Tag,"NOOO data is read from buffer database");
            }
            //send unit data to the server
                //delete unit data from bufferdatabase if the sending is successful
            if(myDeletePermission){
                //db.delete
                Log.d(Tag,"permission granted for deleting a units log");
            }

            }while (C.moveToNext());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //buffer data query ends here for all units*/
    }
};
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (!mGoogleApiClient.isConnecting() || !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }




        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient.isConnecting() || mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    protected boolean isOnLine()
    {
        ConnectivityManager cm= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo=cm.getActiveNetworkInfo();
        if(netInfo!=null&& netInfo.isConnectedOrConnecting())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void populateLandmark(){
       // String[] projection={dbcreate.EMPID,dbcreate.UNITID,dbcreate.LATITUDE,dbcreate.LONGITUDE};
       // Cursor c=db.query(dbcreate.TB_name,projection,null,null,null,null,null);
        dbUnitSuper dbunit=dbUnitSuper.getInstance(this);
        ArrayList<ArrayList<String>> unitInfo=dbunit.readUnitInformation();
    if(unitInfo!=null) {

for(int i=0;i<unitInfo.size();i++)
{
    ArrayList<String> singleUnit=unitInfo.get(i);
    LANDMARKS.put(singleUnit.get(0),
                new LatLng(Float.parseFloat(singleUnit.get(1)), Float.parseFloat(singleUnit.get(2))));}

    }
    }
    public void populateGeofenceList() {
        populateLandmark();
        for (Map.Entry<String, LatLng> entry : LANDMARKS.entrySet()) {
            mGeofenceList.add(new Geofence.Builder()
                    .setRequestId(entry.getKey())
                    .setCircularRegion(
                            entry.getValue().latitude,
                            entry.getValue().longitude,
                            GEOFENCE_RADIUS_IN_METERS

                    )
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setLoiteringDelay(5000)
                    .setNotificationResponsiveness(5000)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_EXIT|Geofence.GEOFENCE_TRANSITION_ENTER)
                    .build());
//Geofence.GEOFENCE_TRANSITION_ENTER  |
        }
        int i=mGeofenceList.size();
        Log.d(Tag,"number of geofences entered in the geofence list is "+i);
    }
   /* protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }*/

    @Override
    public void onConnected(@Nullable Bundle bundle) {
       // Toast.makeText(this, "Google API Client on connected method!", Toast.LENGTH_SHORT).show();
        Log.d(Tag,"entered in onconnected method for googleApiclient");
        try {
            LocationServices.GeofencingApi.addGeofences(
                    mGoogleApiClient,
                    getGeofencingRequest(),
                    getGeofencePendingIntent()
            ).setResultCallback(this); // Result processed in onResult().
        } catch (SecurityException securityException) {
            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
            Log.d(Tag," securityException occured while adding in onconnected");
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }
    private PendingIntent getGeofencePendingIntent() {
        Intent intent = new Intent(sendLocation.this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling addgeoFences()
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onResult(@NonNull Status status) {

    }
    private class pollTask extends AsyncTask<String,Void,String>{
        @Override
        protected String doInBackground(String... params) {


            return null;
        }
    }
    private class getUnitUpdate extends AsyncTask<String,Void,String> implements ResultCallback<Status> {
        HttpsURLConnection https;

        @Override
        protected String doInBackground(String... params) {
            InputStreamReader inputStream = null;
            BufferedReader reader = null;

            StringBuilder newSb=new StringBuilder();
            newSb.append(unitUri);
            newSb.append(params[0]);
            try {
                URL url=new URL(newSb.toString());
               https=(HttpsURLConnection) url.openConnection();

                if(https==null)
                {Log.d(Tag,"urlconnection returns null");}
                https.setRequestMethod("GET");
                https.setDoInput(true);


                int status =https.getResponseCode();
                Log.d(Tag, "getunits Status : " + status);
                inputStream = new InputStreamReader(https.getInputStream());
                reader = new BufferedReader(inputStream);
                StringBuilder message = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    message.append(line);
                }
                return message.toString();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                https.disconnect();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(Tag," updated units are"+s);
            try {
                JSONObject jsonRootObject=new JSONObject(s);
                JSONArray jsonlatArray=jsonRootObject.optJSONArray("Latitude");
                JSONArray jsonlonArray=jsonRootObject.optJSONArray("Longitude");
                JSONArray jsonuniArray=jsonRootObject.optJSONArray("Unit_Id");
                dbUnitSuper unitSuper=dbUnitSuper.getInstance(sendLocation.this);
                ArrayList<String> unitIds=unitSuper.readUnitIds();


                if(jsonuniArray.length()!=unitIds.size()){
                    //String empId= unitSuper.readEmpId();
                    //delete the rows in unit database
                    int del=  unitSuper.deleteAllUnitInfo();
                    Log.d(Tag,"number of deleted unit rows are"+del);

                    //populate the unit database again
                    for(int i=0;i<jsonlatArray.length();i++){
                        String unit_id=jsonuniArray.getString(i);
                        String latitude=jsonlatArray.getString(i);
                        String longitude=jsonlonArray.getString(i);

                        long intest=  unitSuper.insertData(empId,unit_id,latitude,longitude);
                        if(intest<0){
                            // Toast.makeText(MainActivity.this, "insert unsuccessful", Toast.LENGTH_LONG).show();
                            String rt=Integer.toString(i);
                            Log.d(Tag,"insert unsuccessful "+rt);
                        }
                        else{
                            // Toast.makeText(MainActivity.this, "successfully inserted a row", Toast.LENGTH_LONG).show();
                            String rt=Integer.toString(i);
                            Log.d(Tag,"successfully inserted a row "+rt);
                        }
                    }

                /*remove geofences here
                    removeGeofences();
                    //clear mgeofencelist arraylist
                    if(mGeofenceList.size()>0){
                        mGeofenceList.clear();
                    }
                    //recreate the geofences
                    populateGeofenceList();*/

                }else{
                    Log.d(Tag,"no new units are added or deleted");
                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
            /*if (!mGoogleApiClient.isConnecting() || !mGoogleApiClient.isConnected()) {
                Log.d(Tag," entered the if block to recreate the geofences");
                mGoogleApiClient.connect();

            }
            else {
                Log.d(Tag," entered the else block to recreate the geofences");
                try {
                    Log.d(Tag," entered the try block to recreate the geofences");
                    LocationServices.GeofencingApi.addGeofences(
                            mGoogleApiClient,
                            getGeofencingRequest(),
                            getGeofencePendingIntent()
                    ).setResultCallback(this); // Result processed in onResult().
                } catch (SecurityException securityException) {
                    // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
                    Log.d(Tag,"sequrity exception occured at unit updaate while creating neew geofences");
                }
            }*/
        }

        @Override
        public void onResult(@NonNull com.google.android.gms.common.api.Status status) {
            Log.d(Tag,"geofences recreated successfully");
        }
    }
    private void removeGeofences(){
        LocationServices.GeofencingApi.removeGeofences(
                mGoogleApiClient,
                // This is the same pending intent that was used in addGeofences().
                getGeofencePendingIntent()
        ).setResultCallback(this); // Result processed in onResult().
    }

    private class sendLog extends AsyncTask<JSONObject,Void,String>{
        HttpsURLConnection https;

        String loopUnitID;
        @Override
        protected String doInBackground(JSONObject... params) {
            InputStreamReader inputStream = null;
            BufferedReader reader = null;
            JSONObject parseUnitIdJson=params[0];
            JSONObject rows=parseUnitIdJson.optJSONObject("Row");
            loopUnitID=rows.optString("unit_id");
            Log.d(Tag,"the unit id parsed in sendlog is "+loopUnitID);

            String postStr=String.format(params[0].toString());
            try {
                URL url=new URL(logUrl);
                https=(HttpsURLConnection) url.openConnection();

                https.setRequestMethod("POST");
                https.setDoInput(true);
                https.setDoOutput(true);
                OutputStreamWriter writer=new OutputStreamWriter(https.getOutputStream());
                writer.write(postStr);
                writer.flush();
                Log.d(Tag,"data flushed succesfully");
                int status =https.getResponseCode();
                Log.d(Tag, "Status : " + status);
                inputStream = new InputStreamReader(https.getInputStream());
                reader = new BufferedReader(inputStream);
                StringBuilder message = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    message.append(line);
                }
                Log.d(Tag,"sendLog returns"+message.toString());
                return message.toString();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
               https.disconnect();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                dbBufferSuper dbBuffer=dbBufferSuper.getInstance(sendLocation.this);
                JSONObject sendResult=new JSONObject(s);
                JSONArray suck=sendResult.optJSONArray("result");
                Log.d(Tag,"the value read from result is "+suck.get(0));
                if(suck.get(0).equals("success")){
                    Log.d(Tag,"permission granted for deleting a units log i");
                    int status= dbBuffer.deleteRows(loopUnitID);
                    if(status>0){
                        Log.d(Tag,"the number of affected rows in deletebufferrows is "+status);
                    }
                   // myDeletePermission=false;
                    /*myDeletePermission=true;
                    if(myDeletePermission==true){
                        Log.d(Tag,"delete permission is set to true");
                    }else{
                        Log.d(Tag,"delete permission is still false after getting success");
                    }*/
                }else {Log.d(Tag,"success is not extracted properly from the json response of sendlog");}
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d(Tag,"failed to instantiate getattlogs response as json" );
            }
        }
    }
}
