package com.example.tamjid.amsssendloc;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Tamjid Rayhan on 10/10/2016.
 */

public class dbBufferSuper {
    private static String Tag="newtag";
    private static dbBufferSuper instance=null;
    private dbBufferHelper dbBuffer;
    SQLiteDatabase writableBufferDb;

    public dbBufferSuper(Context context){
      dbBuffer =new dbBufferHelper(context);
        writableBufferDb=dbBuffer.getWritableDatabase();
    }
    public static dbBufferSuper getInstance(Context context){
        if(instance==null){
            instance=new dbBufferSuper(context);
        }
        return instance;
    }

    public long insertData(String mac,String unit,int trans,String time){
        Log.d(Tag,"bufferdatabase insesrt gets"+mac+unit+trans+time);

        ContentValues contentValues=new ContentValues();
        contentValues.put(dbBufferHelper.MACID,mac);
        //contentValues.put(bufferLogdb.EMPID,emp);
        contentValues.put(dbBufferHelper.UNITID,unit);
        contentValues.put(dbBufferHelper.TRANS,String.format("%d",trans));
        //contentValues.put(bufferLogdb.DATE,date);
        contentValues.put(dbBufferHelper.TIME,time);
        long id=writableBufferDb.insert(dbBufferHelper.TbName,null,contentValues);
        return id;

    }
    public JSONObject readBufferLog(String loopUnitId) {
        //ArrayList<ArrayList<String>> BufferInfo= new ArrayList<ArrayList<String>>();
        String[] bufferSelection = {loopUnitId};
        JSONObject unitJson = new JSONObject();
        JSONObject logJson = new JSONObject();
        Cursor bufferCursor = writableBufferDb.query(dbBufferHelper.TbName, null, dbBufferHelper.UNITID + " =?", bufferSelection, null, null, null, null);
        try{
        if (bufferCursor.getCount() > 0) {
            //buffer data query loop
            Log.d(Tag, "some data is read from buffer database");
            bufferCursor.moveToFirst();
            int a = 0;
            do {
                String key = String.format("%s", a);
                JSONObject value = new JSONObject();
                String macid = bufferCursor.getString(bufferCursor.getColumnIndex(dbBufferHelper.MACID));
                String trans = bufferCursor.getString(bufferCursor.getColumnIndex(dbBufferHelper.TRANS));
                String datetime = bufferCursor.getString(bufferCursor.getColumnIndex(dbBufferHelper.TIME));
                //entry logs in a json object for a given unit
                value.put("PIN", macid);
                value.put("DateTime", datetime);
                value.put("Verified", "1");
                value.put("Status", trans);
                value.put("WorkCode", "0");
                logJson.put(key, value);
                //this is the last line
                a++;
            }
            while (bufferCursor.moveToNext());
            //buffer data query ends here for a single unit
            //write code for each unit.write the complete json object for a unit here
            logJson.put("unit_id", loopUnitId);
            unitJson.put("Row", logJson);
            Log.d(Tag, "unitjson is " + unitJson.toString());
           return unitJson;

        } else {
            Log.d(Tag, "NOOO data is read from buffer database");
            return null;
        }
        //send unit data to the server
        //delete unit data from bufferdatabase if the sending is successful



    }catch (JSONException e) {
        e.printStackTrace();
            Log.d(Tag,"Jsonexception occured at read bufferlog function");
        }
        return null;
    }
    public int deleteRows(String loopUnitId){
        String[] bufferSelection = {loopUnitId};
       int status= writableBufferDb.delete(dbBufferHelper.TbName,dbBufferHelper.UNITID + " =?",bufferSelection);
        return status;
    }



    static class dbBufferHelper extends SQLiteOpenHelper{
        Context context;
        private static final String DbName="bufferdb";
        private static final String TbName="buffertable";
        private static final int DbVer=1;
        public static final String UID = "ppid";
        public static final String MACID = "macid";
        public static final String UNITID = "unit_id";
        public static final String TRANS = "trans";
        public static final String TIME = "time";
        private static final String CreateTbSql=
                " CREATE TABLE " +TbName+ " (" +UID+ " INTEGER PRIMARY KEY AUTOINCREMENT, " +MACID+ " VARCHAR(255), "
                         +UNITID+ " VARCHAR(255), " +TRANS+ " VARCHAR(255), " +TIME+ " VARCHAR(255));";


        public dbBufferHelper(Context context) {
            super(context, DbName, null, DbVer);
            this.context = context;
            Log.d(Tag, "dbBufferhelpers constructor is called ");
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            Log.d(Tag, "DbBufferHelpers oncreate called");
            try {
                sqLiteDatabase.execSQL(CreateTbSql);
                // db.execSQL(createTB);
            } catch (SQLException e) {
                e.printStackTrace();
                Log.d(Tag,"problem creating the Buffer databases");
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }
    }
}
