package com.example.tamjid.amsssendloc;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;


public class bufferLogdb extends SQLiteOpenHelper {
    public static final String DB_NAME = "bufferDb";
    public static final String TB_name = "bufferTb";
    public static final int DB_VER = 1;
    public static final String UID = "_ID";
    public static final String MACID = "macid";
    //public static final String EMPID = "empid";
    public static final String UNITID = "unit_id";
    public static final String TRANS = "trans";
    //public static final String DATE= "date";
    public static final String TIME = "time";
    public static final String createTB="CREATE TABLE " + TB_name +
            " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + MACID + " VARCHAR(255)," + UNITID + " VARCHAR(255),"+ TRANS + " VARCHAR(255),"+TIME+" VARCHAR(255));";
            //"CREATE TABLE " + TB_name + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + MACID + " VARCHAR(255),"+ UNITID+"VARCHAR(255),"+TRANS+"VARCHAR(255),"+TIME+"VARCHAR(255);";
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TB_name;
    Context context;
    private String Tag="newtag";
    public bufferLogdb(Context context) {
        super(context, DB_NAME, null, DB_VER);
        this.context = context;
        Log.d(Tag, "bufferlogdb constructor is called ");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(Tag, "bufferlogdbs oncreate called");
        Toast.makeText(context, "bufferlogdbs create called", Toast.LENGTH_LONG).show();
        try {
            db.execSQL(createTB);

        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(Tag,"problem creating the buffer database");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL(DROP_TABLE);
            onCreate(db);
            Log.d(Tag, "onUpgrade called");
            Toast.makeText(context, "upgrade called", Toast.LENGTH_LONG).show();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
