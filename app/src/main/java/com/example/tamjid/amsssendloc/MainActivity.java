package com.example.tamjid.amsssendloc;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.NetworkInterface;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    EditText fullname;
    EditText empid;
    EditText employeeId;
    public String regUri="https://abdulhalim.pythonanywhere.com/rams4/default/appUserRegistration.json";
    public String unitUri="https://abdulhalim.pythonanywhere.com/rams4/default/getUnits.json?client_id=";
    //public String regUri="https://abdulhalim.pythonanywhere.com/rams/default/appUserRegistration.json";
   //public String unitUri="https://abdulhalim.pythonanywhere.com/rams/default/getUnits.json?client_id=";
    public String successUri="https://abdulhalim.pythonanywhere.com/rams4/default/getsuccess?unit_id=";
    //public String successUri="https://abdulhalim.pythonanywhere.com/rams/default/getsuccess?unit_id=";
    public String address;
    public String empiId;
    private static String Tag="newtag";
dbUnitSuper dbunitInstance;
    /*dbCreate dbcreate;
    SQLiteDatabase db;
    bufferLogdb bufferLog;
    SQLiteDatabase dbBuffer;*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            //&& checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED
            requestPermissions(new String[] {android.Manifest.permission.ACCESS_COARSE_LOCATION},PERMISSION_REQUEST_COARSE_LOCATION);
        }
        if (!islocationenabled()) {
            Intent intent1=new Intent(this,promptActivity.class);
            //intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent1);}

       // Intent inten=new Intent(MainActivity.this,sendLocation.class);
        //startService(inten);

        fullname=(EditText) findViewById(R.id.editFullName);
        empid= (EditText) findViewById(R.id.empid);
        employeeId= (EditText) findViewById(R.id.rollno);
        Button button= (Button) findViewById(R.id.Register);

        //dbcreate=dbCreate.getInstance(this);
        //db=dbcreate.getWritableDatabase();
        dbunitInstance=dbUnitSuper.getInstance(this);
        address=getMacAddr();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fullname.getText().toString().matches("")||empid.getText().toString().matches("")||employeeId.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "All the fields are required! Please fill them up.", Toast.LENGTH_LONG).show();
                }
                else {
                        String st1=fullname.getText().toString();
                        String st2=empid.getText().toString();
                        String st3=employeeId.getText().toString();
                    empiId=st2;
                if(isOnLine())
                {regTask rT=new regTask();
                        rT.execute(st1,st2,st3);}
                    else {
                    Toast.makeText(MainActivity.this, "Registration failed! Please turn the data connection on.", Toast.LENGTH_LONG).show();
                }


              }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }



    private class regTask extends AsyncTask<String,Void,String>{
   HttpsURLConnection https;

    @Override
    protected String doInBackground(String... params) {
        InputStreamReader inputStream = null;
        BufferedReader reader = null;


        try {
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("fullname",params[0]);
            jsonObject.put("employer_id",params[1]);
            //jsonObject.put("mac_id",address);
            jsonObject.put("mac_id",address);
            jsonObject.put("employee_id",params[2]);

            String str= new String(jsonObject.toString());
            Log.d(Tag,str);
            URL url=new URL(regUri);
            https=(HttpsURLConnection) url.openConnection();

            if(https==null)
            {Log.d(Tag,"urlconnection for regtask returns null");}
            else
            https.setRequestMethod("POST");
            https.setDoInput(true);
            https.setDoOutput(true);
            OutputStreamWriter writer=new OutputStreamWriter(https.getOutputStream());
            writer.write(str);
            writer.flush();
            Log.d(Tag,"data flushed succesfully");
            int status =https.getResponseCode();
            Log.d(Tag, "Status : " + status);
            inputStream = new InputStreamReader(https.getInputStream());
            reader = new BufferedReader(inputStream);
            StringBuilder message = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                message.append(line);
            }
            Log.d(Tag,"appuserregister returns"+message.toString());
            Log.d(Tag,"code is not stopping after printing message");
            return message.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(Tag,"jsonexception occured at regtask doinbackground");
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(Tag,"IOexception occured at regtask doinbackground");
        } finally {
           https.disconnect();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.d(Tag,"code is in onpostexecute of regtask");
        String status="";
        try {
            JSONObject statusJson=new JSONObject(s);
            status=statusJson.getString("status_flag");
            Log.d(Tag,"the retrieved status is "+status);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(Tag,"jsonexception occured while getting status flag");
        }
        if(status.equals("0")){
            Log.d(Tag,"status 0 found");
    Toast.makeText(MainActivity.this, "You are already registered"+status, Toast.LENGTH_LONG).show();
}else if(status.equals("1")){
            Log.d(Tag,"status 1 found");
    Toast.makeText(MainActivity.this, "Registration successful"+status, Toast.LENGTH_LONG).show();
    getUnits getunits=new getUnits();
    getunits.execute();
}else if(status.equals("2")) {
    Toast.makeText(MainActivity.this, "Employer ID does not exist "+status, Toast.LENGTH_LONG).show();
}else {
            Log.d(Tag,"no status condition is attained");
        }
       /* try {
            JSONObject jsonRootObject=new JSONObject(s);
            JSONArray jsonArray=jsonRootObject.optJSONArray("units");
            for(int i=0;i<jsonArray.length();i++){
           JSONObject json =jsonArray.getJSONObject(i);
            String unit_id= json.optString("unit_id").toString();
            String latitude=json.optString("latitude").toString();
            String longitude=json.optString("longitude").toString();
           long intest=  insertData(empiId,unit_id,latitude,longitude);
                if(intest<0){
                    Toast.makeText(MainActivity.this, "insert unsuccessful", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(MainActivity.this, "successfully inserted a row", Toast.LENGTH_LONG).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
    }
}
    private class getUnits extends AsyncTask<String,Void,String>{
        HttpsURLConnection https;

        @Override
        protected String doInBackground(String... params) {
            InputStreamReader inputStream = null;
            BufferedReader reader = null;

            StringBuilder newSb=new StringBuilder();
            newSb.append(unitUri);
            newSb.append(empiId);

Log.d(Tag,"enterd in getunits");
            try {
                URL url=new URL(newSb.toString());
                https=(HttpsURLConnection) url.openConnection();

                if(https==null)
                {Log.d(Tag,"urlconnection returns null");}
                https.setRequestMethod("GET");
                https.setDoInput(true);


                int status =https.getResponseCode();
                Log.d(Tag, "getunits Status : " + status);
                inputStream = new InputStreamReader(https.getInputStream());
                reader = new BufferedReader(inputStream);
                StringBuilder message = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    message.append(line);
                }
                return message.toString();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
               https.disconnect();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(Tag,"units are"+s);
            try {
                JSONObject jsonRootObject=new JSONObject(s);
                JSONArray jsonlatArray=jsonRootObject.optJSONArray("Latitude");
                JSONArray jsonlonArray=jsonRootObject.optJSONArray("Longitude");
                JSONArray jsonuniArray=jsonRootObject.optJSONArray("Unit_Id");
                for(int i=0;i<jsonlatArray.length();i++){
                    String unit_id=jsonuniArray.getString(i);
                    String latitude=jsonlatArray.getString(i);
                    String longitude=jsonlonArray.getString(i);
                    long intest=  dbunitInstance.insertData(empiId,unit_id,latitude,longitude);
                    if(intest<0){
                       // Toast.makeText(MainActivity.this, "insert unsuccessful", Toast.LENGTH_LONG).show();
                        String rt=Integer.toString(i);
                        Log.d(Tag,"insert unsuccessful "+rt);
                    }
                    else{
                       // Toast.makeText(MainActivity.this, "successfully inserted a row", Toast.LENGTH_LONG).show();
                        String rt=Integer.toString(i);
                        Log.d(Tag,"successfully inserted a row "+rt);
                    }
                }
                //int i=sendSuccessReport();
               // Log.d(Tag,"the number of sendSuccess request is "+i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Intent inten=new Intent(MainActivity.this,sendLocation.class);
            startService(inten);
        }
    }
   /* private int sendSuccessReport(){
        ArrayList<String> unitIds=dbunitInstance.readUnitIds();
        int i;
        for(i=0;i<unitIds.size();i++){
            String unitID=unitIds.get(i);
            sendSuccess sendsuccess=new sendSuccess();
            sendsuccess.execute(unitID);
        }
        return i;
    }*/
    private class sendSuccess extends AsyncTask<String,Void,Void>{
        HttpsURLConnection https;

        @Override
        protected Void doInBackground(String... strings) {
            StringBuilder newSb=new StringBuilder();
            newSb.append(successUri);
            newSb.append(strings[0]);
            try {
                URL url=new URL(newSb.toString());
                https=(HttpsURLConnection) url.openConnection();

                if(https==null)
                {Log.d(Tag,"urlconnection returns null");}
                https.setRequestMethod("GET");


                int status =https.getResponseCode();
                Log.d(Tag, "getunits Status : " + status);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                https.disconnect();
            }
            return null;
        }
    }

    protected boolean islocationenabled(){
        LocationManager lm = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}
        if(!gps_enabled || !network_enabled){
            return false;
        }else {return true;}
    }
    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            Log.d(Tag,"exception occured at getting macid from in the main activity and the exception is "+ex);

        }
        return "02:00:00:00:00:00";
    }
    protected boolean isOnLine()
    {
        ConnectivityManager cm= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo=cm.getActiveNetworkInfo();
        if(netInfo!=null&& netInfo.isConnectedOrConnecting())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
   /* private long insertData(String emp,String unit,String lat,String lon){
        Log.d(Tag,"emp id is "+emp);
        Log.d(Tag,"unit id is "+unit);
        Log.d(Tag,"latitude is "+lat);
        Log.d(Tag,"longitude is "+lon);
        ContentValues contentValues=new ContentValues();
        contentValues.put(dbcreate.EMPID,emp);
        contentValues.put(dbcreate.UNITID,unit);
        contentValues.put(dbcreate.LATITUDE,lat);
        contentValues.put(dbcreate.LONGITUDE,lon);
        long id =db.insert(dbcreate.TB_name,null,contentValues);
        return id;
    }*/
}
