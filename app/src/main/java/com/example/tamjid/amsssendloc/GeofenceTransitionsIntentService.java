package com.example.tamjid.amsssendloc;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.nfc.Tag;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Created by Tamjid on 9/28/2016.
 */
public class GeofenceTransitionsIntentService extends IntentService {
    //protected static final String TAG = "GeofenceTransitionsIS";

    dbCreate dbcreate;
    SQLiteDatabase dbBuffer;
    private String empId;
    private static final String Tag="newtag";
    private String macid;

    public GeofenceTransitionsIntentService() {
        super(Tag);  // use TAG to name the IntentService worker thread
        //dbcreate=dbCreate.getInstance(this);
        //dbBuffer=dbcreate.getWritableDatabase();




        //empId=getEmpId();
        macid=getMacAddr();


    }
    @Override
    protected void onHandleIntent(Intent intent) {
       String dattim;
       // bufferLog=new bufferLogdb(GeofenceTransitionsIntentService.this);
       // dbBuffer=bufferLog.getWritableDatabase();
        GeofencingEvent event = GeofencingEvent.fromIntent(intent);
        if (event.hasError()) {
            Log.e(Tag, "GeofencingEvent Error: " + event.getErrorCode());
            return;
        }
        String transitionString =
                GeofenceStatusCodes.getStatusCodeString(event.getGeofenceTransition());
        int transitionInt=event.getGeofenceTransition();
        List triggeringIDs = new ArrayList();
        for (Geofence geofence : event.getTriggeringGeofences()) {
            triggeringIDs.add(geofence.getRequestId());
        }
        String unitIds= TextUtils.join(",", triggeringIDs);
        Log.d(Tag,String.format("%s:%s",transitionString,unitIds));
        dattim=getDateTime();
        //String date= dattim.get(0);
       // String time=dattim.get(1);
        dbBufferSuper dbBuffer=dbBufferSuper.getInstance(this);
        Log.d(Tag,"dbbuffersuper instance created in geofence intent service");
        long insert=dbBuffer.insertData(macid,unitIds,transitionInt,dattim);
        if(insert<0){
            Log.d(Tag,"insert unsuccessful in bufferdatabase");
        }else{
            Log.d(Tag,"successfully inserted a row in bufferdatabase");
        }
    }
    /*private String getEmpId(){
        String[] projection={dbcreate.EMPID};
        Cursor c=dbUnits.query(dbcreate.TB_name,projection,null,null,null,null,null);
        if(c.moveToFirst()){
            return c.getString(c.getColumnIndex(dbcreate.EMPID));
        }
        else {
            return "";
        }
    }*/
    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            Log.d(Tag,"exception occured at getting macid from in the intent service and the exception is "+ex);
        }
        return "02:00:00:00:00:00";
    }
    private String getDateTime(){
       // ArrayList<String> DateTime=new ArrayList<>();
        Calendar rightNow = Calendar.getInstance();
        Log.d("mytime",rightNow.toString());
        String time;
        int year=rightNow.get(Calendar.YEAR);
        int month=rightNow.get(Calendar.MONTH)+1;
        String smonth=String.format("%02d",month);
        int day=rightNow.get(Calendar.DAY_OF_MONTH);
        String sday=String.format("%02d",day);
        int ampm=rightNow.get(Calendar.AM_PM);
        int hour=rightNow.get(Calendar.HOUR_OF_DAY);
        String shour=String.format("%02d",hour);
        int minute=rightNow.get(Calendar.MINUTE);
        String sminute=String.format("%02d",minute);
        int second=rightNow.get(Calendar.SECOND);
        String ssecond=String.format("%02d",second);

            time=String.format(shour+":"+sminute+":"+ssecond);

        String date=String.format(year+"-"+smonth+"-"+sday);
        String datetime=String.format(date+" "+time);
        //DateTime.add(date);
        //DateTime.add(time);
       // Log.d("mytime","date is "+date+" time is "+time);
        return datetime;
    }
    /*private long insertData(String mac,String unit,String trans,String time){
      Log.d(Tag,"bufferdatabase insesrt gets"+mac+unit+trans+time);

        ContentValues contentValues=new ContentValues();
        contentValues.put(dbcreate.MACID,mac);
        //contentValues.put(bufferLogdb.EMPID,emp);
        contentValues.put(dbcreate.UNITID,unit);
        contentValues.put(dbcreate.TRANS,trans);
        //contentValues.put(bufferLogdb.DATE,date);
        contentValues.put(dbcreate.TIME,time);
        long id=dbBuffer.insert(dbcreate.BFTB_name,null,contentValues);
        return id;

    }*/

}
