package com.example.tamjid.amsssendloc;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Tamjid Rayhan on 10/9/2016.
 */

public class dbUnitSuper {
    private static String Tag="newtag";
    private static dbUnitSuper instance=null;
    private dbUnitHelper dbunit;
    SQLiteDatabase writableUnitDb;
    SQLiteDatabase readableUnitDb;
    public dbUnitSuper(Context context){
                  dbunit=new dbUnitHelper(context);
        writableUnitDb=dbunit.getWritableDatabase();
       /* if(writableUnitDb!=null){
            Log.d("tagg","writable database is created");
        }else{
            Log.d(Tag,"writable database is created");}*/
    }
    public static dbUnitSuper getInstance(Context context){
        if(instance == null) {
            instance = new dbUnitSuper(context);
        }
        return instance;
    }
    public long insertData(String emp,String unit,String lat,String lon){
        Log.d(Tag,"emp id is "+emp);
        Log.d(Tag,"unit id is "+unit);
        Log.d(Tag,"latitude is "+lat);
        Log.d(Tag,"longitude is "+lon);

        if(writableUnitDb!=null){
            Log.d("tagg","writable database is created");
        }else{
            Log.d(Tag,"writable database is not not created");}
        ContentValues contentValues=new ContentValues();
        contentValues.put(dbUnitHelper.EmpID,emp);
        contentValues.put(dbUnitHelper.UnitId,unit);
        contentValues.put(dbUnitHelper.Latitude,lat);
        contentValues.put(dbUnitHelper.Longitude,lon);
        long id =writableUnitDb.insert(dbUnitHelper.TbName,null,contentValues);
        return id;
    }
public ArrayList<ArrayList<String>> readUnitInformation(){
    //readableUnitDb=dbunit.getReadableDatabase();
    String[] projection={dbUnitHelper.UnitId,dbUnitHelper.Latitude,dbUnitHelper.Longitude};
    ArrayList<ArrayList<String>> unitInfo= new ArrayList<ArrayList<String>>();
    Cursor c=writableUnitDb.query(dbUnitHelper.TbName,projection,null,null,null,null,null);
if (c.moveToFirst()){
    do{
        ArrayList<String> row=new ArrayList<>();
        row.add(c.getString(c.getColumnIndex(dbUnitHelper.UnitId)));
        row.add(c.getString(c.getColumnIndex(dbUnitHelper.Latitude)));
        row.add(c.getString(c.getColumnIndex(dbUnitHelper.Longitude)));
        unitInfo.add(row);
    }
    while(c.moveToNext());
}else {unitInfo=null;}
    return unitInfo;
}
    public ArrayList<String> readUnitIds(){
        ArrayList<String> unitIds=new ArrayList<>();
        String[] projection={dbUnitHelper.UnitId};
        Cursor c=writableUnitDb.query(dbUnitHelper.TbName,projection,null,null,null,null,null);
        if(c.moveToFirst()){
            do{
                unitIds.add(c.getString(c.getColumnIndex(dbUnitHelper.UnitId)));
            }while (c.moveToNext());
        }
        return unitIds;
    }
    public String readEmpId(){
        String EmpId;
        String[] projection={dbUnitHelper.EmpID};
        Cursor c=writableUnitDb.query(dbUnitHelper.TbName,projection,null,null,null,null,null);
        c.moveToFirst();
        return c.getString(c.getColumnIndex(dbUnitHelper.EmpID));
    }
    public int deleteAllUnitInfo(){
        int status=writableUnitDb.delete(dbUnitHelper.TbName,null,null);
        return status;
    }
    static class dbUnitHelper extends SQLiteOpenHelper{

        Context context;
        private static final String DbName="unitDb";
        private static final String TbName="unittable";
        private static final int DbVer=1;
        private static final String UnCol="pid";
        private static final String UnitId="unitid";
        private static final String EmpID="empid";
        private static final String Latitude="latitude";
        private static final String Longitude="longitude";

        private static final String CreateTbSql=
                " CREATE TABLE " +TbName+ " (" +UnCol+ " INTEGER PRIMARY KEY AUTOINCREMENT, " +EmpID+ " VARCHAR(255), "
                                +UnitId+ " VARCHAR(255), " +Latitude+ " VARCHAR(255), " +Longitude+ " VARCHAR(255));";



        public dbUnitHelper(Context context) {
            super(context, DbName, null, DbVer);

            this.context = context;
            Log.d(Tag, "dbUnitHelpers constructor is called ");
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            Log.d(Tag, "DbUnitHelpers oncreate called");
            try {
                sqLiteDatabase.execSQL(CreateTbSql);
                // db.execSQL(createTB);
            } catch (SQLException e) {
                e.printStackTrace();
                Log.d(Tag,"problem creating the unit databases");
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }
    }
}
